import React from 'react';
import { Link } from 'gatsby';

import Layout from 'components/Layout';
import SEO from 'components/Seo';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Content</h1>
    <Link to="/creative-edge-die-cut">Creative Edge Die Cut</Link>
  </Layout>
);

export default IndexPage;
