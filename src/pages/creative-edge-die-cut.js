import React from 'react';
// import { Link } from 'gatsby';

import Layout from 'components/Layout';
import SEO from 'components/Seo';

const CreativeEdgeDieCutPage = () => (
  <Layout>
    <SEO
      title="Creative Edge Die Cut"
      description="The highest quality die cut press printed cards, now in 94 shapes and sizes."
    />
    <h1>Creative Edge Die Cut</h1>
  </Layout>
);

export default CreativeEdgeDieCutPage;
