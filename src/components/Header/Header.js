import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import './Header.scss';

const Header = ({ siteTitle }) => (
  <header className="Header">
    <div className="Header__container">
      <h1 style={{ margin: 0 }}>
        <Link className="Header__link" to="/">
          {siteTitle}
        </Link>
      </h1>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
