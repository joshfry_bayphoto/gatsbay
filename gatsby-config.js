const path = require('path');

module.exports = {
  siteMetadata: {
    title: `Bay Photo Lab`,
    description: `Professional photographic printing, press printing, canvas wraps, mounting, Metal Prints, and more for pro photographers around the world from our state-of-the-art lab in Santa Cruz, California.`,
    author: `@joshfry`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        src: path.join(__dirname, 'src'),
        components: path.join(__dirname, 'src/components'),
        images: path.join(__dirname, 'src/images'),
        pages: path.join(__dirname, 'src/pages'),
        templates: path.join(__dirname, 'src/templates'),
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsbay`,
        short_name: `gatsbay`,
        start_url: `/`,
        background_color: `#0094c8`,
        theme_color: `#0094c8`,
        display: `minimal-ui`,
        icon: `src/images/gatsbay-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-plugin-offline',
  ],
};
